package config

// Configuration конфигурация приложения
type Configuration struct {
	Server   ServerConfiguration
	Database DatabaseConfiguration
}
