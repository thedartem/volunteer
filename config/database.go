package config

type DatabaseConfiguration struct {
	Host     string
	Port     string
	Username string
	Password string
	Database string
	Migrator bool
	LogMode  bool
}
