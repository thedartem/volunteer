package config

type ServerConfiguration struct {
	IP     string
	Port   string
	Domain string
}
