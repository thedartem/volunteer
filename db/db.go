package db

import (
	"fmt"
	"time"

	"gitlab.com/thedartem/volunteer/model"
	"gitlab.com/thedartem/volunteer/utilites"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func New() (*gorm.DB, error) {
	config := utilites.GetGlobalConfig()

	dsn := config.Database.Username + ":" + config.Database.Password + "@tcp(" + config.Database.Host + ":" + config.Database.Port + ")/" + config.Database.Database + "?charset=utf8&parseTime=True&loc=Local"
	logLevel := logger.Silent
	if config.Database.LogMode {
		logLevel = logger.Info
	}

	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN:                       dsn,   // data source name
		DefaultStringSize:         256,   // default size for string fields
		DisableDatetimePrecision:  true,  // disable datetime precision, which not supported before MySQL 5.6
		DontSupportRenameIndex:    true,  // drop & create when rename index, rename index not supported before MySQL 5.7, MariaDB
		DontSupportRenameColumn:   true,  // `change` when rename column, rename column not supported before MySQL 8, MariaDB
		SkipInitializeWithVersion: false, // auto configure based on currently MySQL version
	}), &gorm.Config{Logger: logger.Default.LogMode(logLevel)})
	if err != nil {
		return nil, err
	}
	sqlDB, err := db.DB()
	sqlDB.Ping()

	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	sqlDB.SetMaxIdleConns(0)

	// SetMaxOpenConns sets the maximum number of open connections to the database.
	sqlDB.SetMaxOpenConns(100)

	// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	sqlDB.SetConnMaxLifetime(time.Hour)

	AutoMigrate(db)

	return db, nil
}

//TODO: err check
func AutoMigrate(db *gorm.DB) {

	config := utilites.GetGlobalConfig()
	if config.Database.Migrator {
		fmt.Println("AutoMigrate...")
		db.AutoMigrate(&model.User{})
		db.AutoMigrate(&model.UserRole{})
		db.AutoMigrate(&model.Role{})
		db.AutoMigrate(&model.UserRecoveryPassword{})
		db.AutoMigrate(&model.Product{})
		db.AutoMigrate(&model.ProductDescription{})
		db.AutoMigrate(&model.ProductImage{})
		db.AutoMigrate(&model.ProductReview{})
		db.AutoMigrate(&model.ProductFavorite{})
		db.AutoMigrate(&model.Specifications{})
		db.AutoMigrate(&model.SpecificationsToProduct{})
		db.AutoMigrate(&model.SpecificationsDescriptions{})
		db.AutoMigrate(&model.SpecificationsToProductDescriptions{})
		db.AutoMigrate(&model.Category{})
		db.Model(&model.Category{}).Association("ParentID").Clear()

		db.AutoMigrate(&model.CategoryDescription{})
		db.AutoMigrate(&model.CategoryImage{})
		db.AutoMigrate(&model.Order{})
		db.AutoMigrate(&model.OrderBody{})
		db.AutoMigrate(&model.OrderStatus{})
		db.AutoMigrate(&model.OrderPayment{})
		db.AutoMigrate(&model.OrderDelivery{})

		db.AutoMigrate(&model.Message{})

		//********* HUBBER **********

		db.AutoMigrate(&model.Supplier{})
		db.AutoMigrate(&model.SupplierCategory{})
		db.AutoMigrate(&model.SupplierProduct{})
		db.AutoMigrate(&model.SupplierProductPrice{})
		db.AutoMigrate(&model.SupplierProductPicture{})
		db.AutoMigrate(&model.SupplierProductOptions{})
		db.AutoMigrate(&model.SupplierProductParam{})
		db.AutoMigrate(&model.SupplierProductParamValue{})

		//********* FINANCE **********//
		//
		/*db.AutoMigrate(&model.Currency{})

		//Cashboxes
		db.AutoMigrate(&model.CashBox{})            //Касса
		db.AutoMigrate(&model.CashBoxAppointment{}) //Назначение платежа

		db.AutoMigrate(&model.CashOrderIn{})  //Приходный кассовый ордер
		db.AutoMigrate(&model.CashOrderOut{}) //Расходный кассовый ордер

		db.AutoMigrate(&model.Stock{})             //Склад
		db.AutoMigrate(&model.StockMovingHeader{}) //Перемещение товаров
		db.AutoMigrate(&model.StockMovingBody{})   //Перемещение товаров

		db.AutoMigrate(&model.SupplierOrderHeader{}) //Заказ поставщику шапка
		db.AutoMigrate(&model.SupplierOrderBody{})   //Заказ поставщику тело

		db.AutoMigrate(&model.ReceiptInvoiceHeader{}) //Поступление товаров шапка
		db.AutoMigrate(&model.ReceiptInvoiceBody{})   //Поступление товаров тело

		db.AutoMigrate(&model.SalesInvoiceBody{})   //Расход товаров тело
		db.AutoMigrate(&model.SalesInvoiceHeader{}) //Расход товаров шапка
		*/
		fmt.Print(" Done.")
		fmt.Println("")
	}

}
