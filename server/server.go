package server

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"gitlab.com/thedartem/volunteer/db"
	"gitlab.com/thedartem/volunteer/handler"
	"gitlab.com/thedartem/volunteer/store"
	"gitlab.com/thedartem/volunteer/utilites"
)

func New() {
	e := echo.New()
	e.Logger.SetLevel(log.DEBUG)
	e.Pre(middleware.RemoveTrailingSlash())
	e.Use(middleware.Logger())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAuthorization, echo.HeaderAccessControlAllowHeaders, echo.HeaderAccessControlAllowOrigin},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))

	e.Validator = NewValidator()
	e.HideBanner = true

	db, err := db.New()
	if err != nil {
		log.Panic(err.Error)
	}
	//defer db.Close()

	s := store.New(db)
	handler.New(e, s)

	config := utilites.GetGlobalConfig()

	/*e.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Level: 9,
	}))*/

	e.Logger.Fatal(e.Start(config.Server.IP + ":" + config.Server.Port))
}
