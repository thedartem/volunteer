package middleware

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/thedartem/volunteer/utilites"

	//Костыл для старого кода, очень много зашито на этих переменных

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
)

type (
	JWTConfig struct {
		Skipper    Skipper
		SigningKey interface{}
	}
	Skipper      func(c echo.Context) bool
	jwtExtractor func(echo.Context) (string, error)
)

var (
	ErrJWTMissing = echo.NewHTTPError(http.StatusUnauthorized, "missing or malformed jwt")
	ErrJWTInvalid = echo.NewHTTPError(http.StatusForbidden, "invalid or expired jwt")
)

func JWT(key interface{}, rolesList []string) echo.MiddlewareFunc {
	c := JWTConfig{}
	c.SigningKey = key
	return JWTWithConfig(c, rolesList)
}

func JWTWithConfig(config JWTConfig, rolesList []string) echo.MiddlewareFunc {
	extractor := jwtFromHeader("Authorization", "Token")
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			auth, err := extractor(c)
			if err != nil {
				if config.Skipper != nil {
					if config.Skipper(c) {
						return next(c)
					}
				}
				return c.JSON(http.StatusUnauthorized, utilites.NewError(err))
			}
			token, err := jwt.Parse(auth, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
				}
				return config.SigningKey, nil
			})
			if err != nil {
				return c.JSON(http.StatusForbidden, utilites.NewError(ErrJWTInvalid))
			}
			if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
				userID := uint(claims["id"].(float64))
				//fmt.Println("TOKEN CLAIM", claims["id"])
				c.Set("user_id", userID)
				role := claims["role"].(string)
				roles := strings.Split(role, ",")
				c.Set("role", roles)

				if rolesList != nil {
					if role != "" {
						for _, ri := range roles {
							for _, r := range rolesList {
								if ri == r {
									return next(c)
								}
							}
						}
					} else {
						return c.JSON(http.StatusForbidden, utilites.NewError(ErrJWTInvalid))
					}
				}

				return next(c)
			}
			return c.JSON(http.StatusForbidden, utilites.NewError(ErrJWTInvalid))
		}
	}
}

// jwtFromHeader returns a `jwtExtractor` that extracts token from the request header.
func jwtFromHeader(header string, authScheme string) jwtExtractor {
	return func(c echo.Context) (string, error) {
		auth := c.Request().Header.Get(header)
		l := len(authScheme)
		if len(auth) > l+1 && auth[:l] == authScheme {
			return auth[l+1:], nil
		}
		return "", ErrJWTMissing
	}
}
