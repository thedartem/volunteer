package utilites

import (
	"fmt"
	"strconv"
	"strings"
)

func parseVersion(s string, width int64) int64 {
	strList := strings.Split(s, ".")
	format := fmt.Sprintf("%%s%%0%ds", width)
	v := ""
	for _, value := range strList {
		v = fmt.Sprintf(format, v, value)
	}
	var result int64
	var err error
	if result, err = strconv.ParseInt(v, 10, 64); err != nil {
		fmt.Printf("ugh: parseVersion(%s): error=%s", s, err)
		return 0
	}
	fmt.Printf("parseVersion: [%s] => [%s] => [%d]\n", s, v, result)
	return result
}

func CheckVersion(currentVersion, minVersion string, maxVersion *string) bool {
	var ctv, mnv, mxv int64

	ctv = parseVersion(currentVersion, 4)
	mnv = parseVersion(minVersion, 4)

	if maxVersion != nil {
		mxv = parseVersion(*maxVersion, 4)
		if ctv >= mnv && ctv <= mxv {
			return true
		}
	} else {
		if ctv >= mnv {
			return true
		}
	}

	return false
}
