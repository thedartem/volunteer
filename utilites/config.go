package utilites

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"gitlab.com/thedartem/volunteer/config"

	"github.com/kardianos/osext"
	"github.com/spf13/viper"
)

var dev bool
var globalConfig *config.Configuration

func GetGlobalConfig() *config.Configuration {
	var (
		dir string
		err error
	)
	if globalConfig != nil {
		return globalConfig
	}

	if _, err = os.Stat("./data/configs/config.json"); os.IsNotExist(err) {
		dir, err = osext.ExecutableFolder()
		if err != nil {
			dir = "."
		} else {
			dir = dir + "/data"
		}

	} else {
		dir, err = filepath.Abs(filepath.Dir("./data/configs/config.json"))
		if err != nil {
			log.Fatal(err)
		}
		//fmt.Println(dir)
	}

	//fmt.Println("dir >>>>", dir)
	v := viper.New()
	v.SetConfigType("json")
	v.SetConfigName("config")
	v.AddConfigPath(dir + "/")
	//v.AddConfigPath("./data/")
	cfg := new(config.Configuration)
	if err = v.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}
	err = v.Unmarshal(&cfg)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}
	// log.Printf("database uri is %s", cfg)
	// log.Printf("port for this application is %d", configuration.Server.Port)

	if cfg.Server.IP == "" {
		cfg.Server.IP = "127.0.0.1"
	}

	if cfg.Server.Port == "" {
		cfg.Server.Port = "5000"
	}

	if cfg.Database.Host == "" {
		cfg.Database.Host = "127.0.0.1"
	}

	if cfg.Database.Port == "" {
		cfg.Database.Port = "3306"
	}

	if cfg.Database.Username == "" || cfg.Database.Password == "" || cfg.Database.Database == "" {
		log.Fatalf("Empty settings database.")
	}

	globalConfig = cfg
	return cfg
}

func SetDev(mode bool) bool {
	dev = mode
	return dev
}

func GetDev() bool {
	return dev
}

func SaveGlobalConfig(c *config.Configuration) error {
	var (
		dir string
		err error
	)
	if _, err = os.Stat("./data/configs/config.json"); os.IsNotExist(err) {
		dir, err = osext.ExecutableFolder()
		if err != nil {
			dir = "."
		} else {
			dir = dir + "/data"
		}

	} else {
		dir, err = filepath.Abs(filepath.Dir("./data/configs/config.json"))
		if err != nil {
			log.Fatal(err)
		}
		//fmt.Println(dir)
	}

	globalConfig = c
	file, _ := json.MarshalIndent(c, "", " ")

	return ioutil.WriteFile(dir+"/config.json", file, 0644)
}

func CreateGlobalConfig() error {
	var (
		cfg config.Configuration
	)

	if len(cfg.Security.CORS) == 0 {
		cfg.Security.CORS = append(cfg.Security.CORS, "*")
	}

	if cfg.Server.IP == "" {
		cfg.Server.IP = "127.0.0.1"
	}

	if cfg.Server.Port == "" {
		cfg.Server.Port = "5000"
	}

	if cfg.Database.Host == "" {
		cfg.Database.Host = "127.0.0.1"
	}

	if cfg.Database.Port == "" {
		cfg.Database.Port = "3306"
	}

	/*if _, err = os.Stat("./data/configs/config.json"); os.IsNotExist(err) {
		dir, err = osext.ExecutableFolder()
		if err != nil {
			dir = "."
		} else {
			dir = dir + "/data/configs/"
		}

	} else {
		dir, err = filepath.Abs(filepath.Dir("./data/configs/config.json"))
		if err != nil {
			log.Fatal(err)
		}
		//fmt.Println(dir)
	}*/

	dir := "./data/configs/"
	if _, err := os.Stat(dir + "config.json"); os.IsNotExist(err) {
		err := os.MkdirAll(dir, os.ModePerm)
		if err != nil {
			return err
		}
	}

	file, _ := json.MarshalIndent(cfg, "", " ")

	return ioutil.WriteFile(dir+"config.json", file, 0644)
}
