package utilites

import (
	"net/url"
	"strings"

	translit "pkg.re/essentialkaos/translit.v2"
)

func CreateURL(value string) string {
	var link string

	value = strings.TrimSpace(value)
	value = strings.ToLower(value)

	value = strings.NewReplacer("\"", "").Replace(value)
	value = strings.NewReplacer("'", "").Replace(value)
	value = strings.NewReplacer("%", "").Replace(value)
	value = strings.NewReplacer("*", "").Replace(value)
	value = strings.NewReplacer(" ", "-").Replace(value)
	value = strings.Replace(value, " ", "-", -1)
	value = strings.NewReplacer(",", "").Replace(value)
	value = strings.NewReplacer(".", "").Replace(value)
	value = strings.NewReplacer(";", "").Replace(value)
	value = strings.NewReplacer("`", "").Replace(value)
	value = strings.NewReplacer("~", "").Replace(value)
	value = strings.NewReplacer("$", "").Replace(value)
	value = strings.NewReplacer("<", "").Replace(value)
	value = strings.NewReplacer(">", "").Replace(value)
	value = strings.NewReplacer("/", "").Replace(value)
	value = strings.NewReplacer("!", "").Replace(value)
	value = strings.NewReplacer("|", "").Replace(value)
	value = strings.NewReplacer("@", "").Replace(value)
	value = strings.NewReplacer("#", "").Replace(value)
	value = strings.NewReplacer("№", "").Replace(value)
	value = strings.NewReplacer("&", "-and-").Replace(value)
	value = strings.NewReplacer("i", "i").Replace(value)
	value = strings.NewReplacer("і", "i").Replace(value)
	value = strings.NewReplacer("ї", "i").Replace(value)
	value = strings.NewReplacer("(", "").Replace(value)
	value = strings.NewReplacer(")", "").Replace(value)
	value = strings.NewReplacer("°", "").Replace(value)
	value = strings.NewReplacer("?", "").Replace(value)

	link = translit.Universal(value)
	link = url.QueryEscape(link)

	return link
}
