package utilites

import (
	"fmt"
	"io"
	"net/url"
	"os"
	"strings"

	translit "pkg.re/essentialkaos/translit.v2"
)

func ValidateFile(value string) string {
	var link string

	value = strings.TrimSpace(value)
	value = strings.ToLower(value)

	value = strings.NewReplacer("\"", "").Replace(value)
	value = strings.NewReplacer("'", "").Replace(value)
	value = strings.NewReplacer("%", "").Replace(value)
	value = strings.NewReplacer("*", "").Replace(value)
	value = strings.NewReplacer(" ", "-").Replace(value)
	value = strings.Replace(value, " ", "-", -1)
	value = strings.NewReplacer(",", "").Replace(value)
	value = strings.NewReplacer(";", "").Replace(value)
	value = strings.NewReplacer("`", "").Replace(value)
	value = strings.NewReplacer("~", "").Replace(value)
	value = strings.NewReplacer("$", "").Replace(value)
	value = strings.NewReplacer("<", "").Replace(value)
	value = strings.NewReplacer(">", "").Replace(value)
	value = strings.NewReplacer("/", "").Replace(value)
	value = strings.NewReplacer("!", "").Replace(value)
	value = strings.NewReplacer("|", "").Replace(value)
	value = strings.NewReplacer("@", "").Replace(value)
	value = strings.NewReplacer("#", "").Replace(value)
	value = strings.NewReplacer("№", "").Replace(value)
	value = strings.NewReplacer("&", "-and-").Replace(value)
	value = strings.NewReplacer("i", "y").Replace(value)
	value = strings.NewReplacer("і", "y").Replace(value)
	value = strings.NewReplacer("ї", "y").Replace(value)

	link = translit.Universal(value)
	link = url.QueryEscape(link)

	return link
}

func FileCopy(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}
