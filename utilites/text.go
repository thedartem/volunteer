package utilites

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strings"

	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
)

func TextOnlyNumber(s string) string {
	reg, err := regexp.Compile("[^0-9]+")
	if err != nil {
		log.Fatal(err)
	}
	processedString := reg.ReplaceAllString(s, "")
	return processedString
}

func EncodeWindows1251(b []byte) []byte {
	enc := charmap.Windows1251.NewEncoder()
	out, _ := enc.Bytes(b)
	return out
}

func EncodeWindows1251String(s string) string {
	sr := strings.NewReader(s)
	tr := transform.NewReader(sr,
		charmap.Windows1251.NewEncoder())
	buf, err := ioutil.ReadAll(tr)
	if err != nil {
		fmt.Print(err)
	}

	return string(buf)
}

func CutText(text string, limit int) string {
	runes := []rune(text)
	if len(runes) >= limit {
		return string(runes[:limit])
	}
	return text
}
