package utilites

func GetOffsetLimitFromPage(page, itemOnPage int) (offset, limit int) {
	offset = -1
	limit = -1

	if itemOnPage > 0 {
		limit = itemOnPage
	}

	page = page - 1

	if page > 0 {
		offset = page * itemOnPage
	}

	return offset, limit
}
