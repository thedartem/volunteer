package utilites

import "math/rand"

// RandInt
func RandInt(min int, max int) int {
	return min + rand.Intn(max-min)
}
