package utilites

import (
	"errors"
	"fmt"
	"net/smtp"
)

func SendMail(to []string, theme, text string) error {
	config := GetGlobalConfig()

	if config.Mail.Host == "" || config.Mail.Email == "" || config.Mail.Password == "" {
		return errors.New("Отправка почты не настроенна.")
	}

	if len(to) == 0 {
		return errors.New("Список получателей пуст.")
	}

	body := `To: "Client" <` + to[0] + `>
From: "` + config.Mail.From + `" <` + config.Mail.Email + `>
Subject: ` + theme + `
MIME-version: 1.0;
Content-Type: text/html; 

` + text + `
`

	SMTP := fmt.Sprintf("%s:%d", config.Mail.Host, config.Mail.Port)
	auth := smtp.PlainAuth("", config.Mail.Email, config.Mail.Password, config.Mail.Host)
	if err := smtp.SendMail(SMTP, auth, config.Mail.Email, to, []byte(body)); err != nil {
		return err
	}
	return nil
	/*
		d := mail.Dialer{Host: config.Mail.Host, Port: config.Mail.Port, StartTLSPolicy: mail.NoStartTLS}
		msg := mail.NewMessage()
		msg.SetHeader("From", config.Mail.Email)
		msg.SetHeader("To", to...)
		msg.SetHeader("Subject", theme)
		msg.SetBody("text/html", text)
		if err := d.DialAndSend(msg); err != nil {
			return errors.New("Не удалось отправить сообщение. " + err.Error())
		}

		return nil*/
}

func SendMailFromSender(to []string, senderName, senderEmail, theme, text string) error {
	config := GetGlobalConfig()

	if config.Mail.Host == "" || config.Mail.Email == "" || config.Mail.Password == "" {
		return errors.New("Отправка почты не настроенна.")
	}

	if len(to) == 0 {
		return errors.New("Список получателей пуст.")
	}

	body := `To: "Client" <` + to[0] + `>
From: "` + senderName + `" <` + senderEmail + `>
Subject: ` + theme + `
MIME-version: 1.0;
Content-Type: text/html; 

` + text + `
`

	SMTP := fmt.Sprintf("%s:%d", config.Mail.Host, config.Mail.Port)
	auth := smtp.PlainAuth("", config.Mail.Email, config.Mail.Password, config.Mail.Host)
	if err := smtp.SendMail(SMTP, auth, config.Mail.Email, to, []byte(body)); err != nil {
		return err
	}
	return nil
	/*
		d := mail.Dialer{Host: config.Mail.Host, Port: config.Mail.Port, StartTLSPolicy: mail.NoStartTLS}
		msg := mail.NewMessage()
		msg.SetHeader("From", config.Mail.Email)
		msg.SetHeader("To", to...)
		msg.SetHeader("Subject", theme)
		msg.SetBody("text/html", text)
		if err := d.DialAndSend(msg); err != nil {
			return errors.New("Не удалось отправить сообщение. " + err.Error())
		}

		return nil*/
}
