package utilites

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

type Message struct {
	Destination string `json:"destination"`
	Message     string `json:"message"`
}

// https://alphasms.ua/api/http.php?version=http&login=LOGIN&password=PASS&command=bal
// ance

func GetBalance() error {

	settings := GetGlobalConfig()

	if settings.SMS.Service == "alphasms" {
		var Url *url.URL
		Url, err := url.Parse("https://alphasms.ua")
		if err != nil {
			return err
		}

		Url.Path += "/api/http.php"
		parameters := url.Values{}
		parameters.Add("version", "http")
		parameters.Add("key", settings.SMS.APIKey)
		parameters.Add("command", "balance")
		Url.RawQuery = parameters.Encode()

		// link := "https://alphasms.ua/api/http.php?version=http&key=" + settings.APIKey + "&command=send&from=" + settings.Sender + "&to=" + number + "&message=" + msg

		resp, err := http.Get(Url.String())
		if err != nil {
			log.Println(err)
			return err
		}

		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		bodyString := string(body)

		fmt.Println("Alphasms response", bodyString)
	}

	return nil
}
func SendSMSMessage(phone, msg string) error {
	settings := GetGlobalConfig()

	number := phone
	// re := regexp.MustCompile("[^0-9+-]")
	re := regexp.MustCompile("^(38)[\\d]{10}$")
	number = strings.Join(re.FindAllString(number, -1), "")
	if len(number) != 12 {
		return fmt.Errorf("Неверный номер телефона должно быть 12 чисел из которых первые две 38")
	}

	if settings.SMS.Service == "alphasms" {
		var Url *url.URL
		Url, err := url.Parse("https://alphasms.ua")
		if err != nil {
			return err
		}

		Url.Path += "/api/http.php"
		parameters := url.Values{}
		parameters.Add("version", "http")
		parameters.Add("key", settings.SMS.APIKey)
		parameters.Add("command", "send")
		parameters.Add("from", settings.SMS.Sender)
		parameters.Add("to", number)
		parameters.Add("message", msg)
		Url.RawQuery = parameters.Encode()

		// link := "https://alphasms.ua/api/http.php?version=http&key=" + settings.APIKey + "&command=send&from=" + settings.Sender + "&to=" + number + "&message=" + msg

		resp, err := http.Get(Url.String())
		if err != nil {
			log.Println(err)
			return err
		}

		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		bodyString := string(body)

		fmt.Println("Alphasms response", bodyString)
	}

	return nil
}
