package utilites

import (
	"strings"
	"time"

	"gitlab.com/thedartem/volunteer/model"

	"github.com/dgrijalva/jwt-go"
)

var JWTSecret = []byte("GoCommerceSecretFraze")

func GenerateJWT(m *model.User) string {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["id"] = m.ID
	var roles []string
	for _, i := range m.Role {
		roles = append(roles, i.Role.Name)
	}
	claims["role"] = strings.Join(roles, ",")
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
	t, _ := token.SignedString(JWTSecret)
	//fmt.Println("ID", m.ID)
	return t
}
