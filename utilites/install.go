package utilites

import (
	"fmt"
	"log"
	"os"

	"github.com/kardianos/osext"
)

func CheckInstall() bool {
	var (
		dir string
		err error
	)
	if _, err = os.Stat("./data/"); os.IsNotExist(err) {
		dir, err = osext.ExecutableFolder()
		if err != nil {
			dir = "."
		} else {
			dir = dir + "/data/"
		}

	} else {
		return false
	}

	fmt.Println("Установка GO COMMERCE")

	if _, err = os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, os.ModePerm)
		if err != nil {
			log.Panicln("Error", err.Error())
			return true
		}
	}

	if _, err = os.Stat(dir + "public/"); os.IsNotExist(err) {
		err = os.MkdirAll(dir+"public/", os.ModePerm)
		if err != nil {
			log.Panicln("Error", err.Error())
			return true
		}
	}
	/*if _, err = os.Stat(dir + "images/"); os.IsNotExist(err) {
		err = os.MkdirAll(dir+"images/", os.ModePerm)
		if err != nil {
			log.Panicln("Error", err.Error())
			return true
		}
	}
	if _, err = os.Stat(dir + "images/pages/"); os.IsNotExist(err) {
		err = os.MkdirAll(dir+"images/pages/", os.ModePerm)
		if err != nil {
			log.Panicln("Error", err.Error())
			return true
		}
	}*/

	err = CreateGlobalConfig()
	if err != nil {
		log.Panicln("Error", err.Error())
		return true
	}

	return true
}
